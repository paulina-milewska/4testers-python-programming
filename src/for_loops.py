import uuid


def print_10_random_uuids(number_of_strings):
    for i in range(number_of_strings):
     print(str(uuid.uuid4()))


if __name__ == '__main__':
    print_10_random_uuids(2)


# Excercise 1
def print_each_student_name_capitalized(list_of_student_first_names):
    for first_name in list_of_student_first_names:
        print(first_name.capitalize())


def print_first_ten_integers_squared():
    for integer in range(1, 11):
        print(integer ** 2)

def print_temp_in_both_scales(list_of_temps_in_celsius):
    for temp in list_of_temps_in_celsius:
        print(f'Celsius: {temp}, Fahrenheit: {temp * 9 / 5 + 32}')

if __name__ == '__main__':
    list_of_students = ["kate", "marek", "tosia", "nicki", "stefka"]
    print_each_student_name_capitalized(list_of_students)
    print_first_ten_integers_squared()
    temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print_temp_in_both_scales(temps_celsius)

# Excercise 3
def print_numbers_divisible_by_7(from_number, to_number):
    for number in range(from_number, to_number + 1):
        if number % 7 == 0:
            print(number)
if __name__ == '__main__':
    print_numbers_divisible_by_7(1, 30)
# Excercise 4
def print_numbers_from_20_to_30():
    for number in range(20, 31):
        print(number)

if __name__ == '__main__':
    print_numbers_from_20_to_30()
