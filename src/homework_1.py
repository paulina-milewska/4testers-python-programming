# Exercise 1
def get_number_squared(num):
    return num ** 2


zero_squared = get_number_squared(0)
print(zero_squared)

sixteen_squared = get_number_squared(16)
print(sixteen_squared)

floating_number_squared = get_number_squared(2.55)
print(floating_number_squared)


# Exercise 2
def cuboid_volume(a, b, c):
    return a * b * c


print(cuboid_volume(3, 5, 7))


# Exercise 3
def convert_temp_degrees_to_fahrenheit(temp_in_celsius):
    return temp_in_celsius * 9 / 5 + 32


fahrenheit = convert_temp_degrees_to_fahrenheit(20)
print(fahrenheit)
