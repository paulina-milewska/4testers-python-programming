movies = ["Mission Impossible", "IronMan", "Transporter 2"]
first_movie = movies[0]
print(f'First movie is {first_movie}')


restaurants = ["KFC", "Burger King"]
print("KFC")

movies.append("Dunne")
print(f'The length of movies list is {len(movies)}')

movies.insert(0, "IronMan")
movies.remove("Transporter 2")
print(movies)

movies[-1] = "Dunne (1980)"
print(movies)

# Excercise 1
emails = ["paulina45@autograf.pl", "milewska.paulina@interia.pl"]

print(f'The length of emails list is {len(emails)}')
first_email = emails[0]
last_email = emails[-1]
emails.insert(0, "danka@wp.pl")
print(emails)
print(first_email)
print(last_email)

# dictionary
friend = {
    "name": "Paulina",
    "age": 37,
    "hobbies": ["dating", "eating"]
    }
print(friend["age"])
friend["city"] = "Wrocław"
friend["age"] = 39
print(friend)
print(friend["hobbies"][-1])

# Excercise 2

def get_list_element_sum(list_with_numbers):
    return sum(list_with_numbers)

temperatues_in_january = [-4, 1.0, -7, 2]
temperatures_in_february = [-13, -9, -3, 3]

print(f'January: {get_list_element_sum(temperatues_in_january)}')
print(f'February: {get_list_element_sum(temperatures_in_february)}')
