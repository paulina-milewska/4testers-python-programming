def print_temperature_description(temperature_in_celsius):
    print(f"Temperature right now is {temperature_in_celsius} Celsius degree")
    if temperature_in_celsius > 25:
        print("It's getting hot!")
    elif temperature_in_celsius > 0:
        print("It's quite OK.")
    else:
        print("It's getting cold :(")


def is_person_an_adult(age):
    if age >= 18:
        return True
    else:
        return False


# return True if age >= 18 else False

if __name__ == '__main__':
    temperature_in_celsius = 25.1
    print_temperature_description(temperature_in_celsius)
    age_kate = 17
    age_tom = 18
    age_marta = 21
    print("Kate", is_person_an_adult(age_kate))
    print("Tom", is_person_an_adult(age_tom))
    print("Marta", is_person_an_adult(age_marta))

# Excercise 1

if __name__ == '__main__':
    name = "Pau"

    if len(name) > 5:
        print(f'The name {name} is longer y=then 5 characters')
    else:
        print(f'The name {name} is 5 characters or shorter')


# Excercise 2
def check_temp_press(temparature_in_celsius, pressure_in_hp):
    if temparature_in_celsius == 0 and pressure_in_hp == 1013:
        return True
    else:
        return False


print(check_temp_press(0, 1013))
print(check_temp_press(0, 1014))
print(check_temp_press(1, 1014))


# Excercise 3
def get_grade_for_exam_points(number_of_points):
    if number_of_points >= 90:
        return 5
    elif number_of_points >= 75:
        return 4
    elif number_of_points >= 50:
        return 3
    else:
        return 2


if __name__ == '__main__':
    print(get_grade_for_exam_points(91))
    print(get_grade_for_exam_points(90))
    print(get_grade_for_exam_points(89))
    print(get_grade_for_exam_points(76))
    print(get_grade_for_exam_points(75))
    print(get_grade_for_exam_points(74))
    print(get_grade_for_exam_points(50))
    print(get_grade_for_exam_points(49))

if __name__ == '__main__':

  def is_it_4testers_day(day_of_week):
    if day_of_week == 'monday' or day_of_week == 'wednesday' or day_of_week == 'friday':
        return True
    else:
        return False


def is_it_japanese_car_brand(car_brand):
    if car_brand in ['Toyota', 'Suzuki', 'Mazda']:
        return True
    else:
        return False


if __name__ == '__main__':
    print(is_it_japanese_car_brand('BMW'))
    print(is_it_japanese_car_brand('KIA'))
    print(is_it_japanese_car_brand('Toyota'))
