# Exercise 1 - Print welcome message to a person in a city
def print_hello_message(name, city):
    return f"Witaj {name}! Miło cię widzieć w naszym mieście: {city}!"

print(print_hello_message("Michał", "Toruń"))
print(print_hello_message("Beata", "Gdynia"))

# Exercise 2 - Generate email in 4testers.pl domain using first nad last name
def get_email(surname, name):
        return f"{name.lower()}.{surname.lower()}@4testers.pl"

print(get_email("Nowak", "Janusz"))
print(get_email("Kowalska", "Barbara"))

# Exercise 2
my_name = "Paulina"
favourite_movie = "Star Wars"
favourite_actor = "Clint Eastwood"

print("My name is NAME. My favourite movie is MOVIE. My favourite actor is ACTOR")

bio = f"My name is {my_name}. My favourite movie is {favourite_movie}. My favourite actor is {favourite_actor}."
print(bio)

# Exercise 3
calculation_example = f"Sum of 2 and 4 is {2 + 4}."
print(calculation_example)
