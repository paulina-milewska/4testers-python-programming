def get_country_of_a_car_brand(car_brand):
    car_brand_lowercase = car_brand.lower()
    if car_brand_lowercase in ('toyota', 'mazda', 'suzuki', 'subaru'):
        return 'Japan'
    elif car_brand_lowercase in ('bmw', 'mercedes', 'audi', 'volkswagen'):
        return 'Germany'
    elif car_brand_lowercase in ('renault', 'peugeot'):
        return 'France'
    else:
        return 'Unknown'


def test_get_country_for_a_japanese_car():
    assert get_country_of_a_car_brand('Toyota') == 'Japan'


def test_get_country_for_a_japanese_car_lowercase():
    assert get_country_of_a_car_brand('toyota') == 'Japan'


def test_get_country_for_a_german_car():
    assert get_country_of_a_car_brand('Mercedes') == 'Germany'


def test_get_country_for_a_german_car_lowercase():
    assert get_country_of_a_car_brand('mercedes') == 'Germany'


def test_get_country_for_a_french_car_():
    assert get_country_of_a_car_brand('Peugeot') == 'France'


def test_get_country_for_a_french_car_lowercase():
    assert get_country_of_a_car_brand('peugeot') == 'France'


def test_get_country_for_a_italian_car():
    assert get_country_of_a_car_brand('fiat') == 'Unknown'


# Tests for a function calculating gas usage
def get_gas_usage_for_distance(distance_in_kilometers, average_gas_usage_per_100_km):
    if distance_in_kilometers < 0 or average_gas_usage_per_100_km < 0:
        return 0
    return distance_in_kilometers / 100 * average_gas_usage_per_100_km

def test_gas_usage_for_zero_distance_driven():
    assert get_gas_usage_for_distance(0, 8.5) == 0


def test_gas_usage_for_100_kilometers_distance_driven():
    assert get_gas_usage_for_distance(100, 8.5) == 8.5


def test_gas_usage_for_negative_distance_driven():
    assert get_gas_usage_for_distance(-50, 8.5) == 0



