def convert_from_celsius_to_fahrenheit(list_temps_in_celsius):
    temps_fahrenheit = []
    for temp in list_temps_in_celsius:
        temps_fahrenheit.append(round(temp * 9 / 5 + 32, 2))
    return temps_fahrenheit


if __name__ == '__main__':
    temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print(convert_from_celsius_to_fahrenheit(temps_celsius))