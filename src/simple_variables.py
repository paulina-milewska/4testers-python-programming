my_name = "Paulina"
my_age = 36
my_email = "milewska.paulina@interia.pl"


print(my_name)
print(my_age)
print(my_email)

# Below is description of my friend
name = "Paulina"
age = 37
pets = 1
friend_has_driving_licence = True
friendship_years = 10

print(name)
print(age)
print(pets)
print(friend_has_driving_licence)
print(friendship_years)
