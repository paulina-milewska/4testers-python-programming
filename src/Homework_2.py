import uuid

# Exercise 1

def get_sum_of_list(list_with_numbers):
    return sum(list_with_numbers)


def get_average_of_two_num(no1, no2):
    return (no1 + no2) / 2


print(get_average_of_two_num(2, 3))

january = [-4, 1.0, -7, 2]
february = [-13, -9, -3, 3]

average_temperature = get_average_of_two_num(get_sum_of_list(january), get_sum_of_list(february))

print(average_temperature)


# Exercise 2

def get_user_credentials(email):
    user_credentials_dict = {
        'email': email,
        'password': str(uuid.uuid4())
    }
    return user_credentials_dict


print(f'User credentials: {get_user_credentials("user@example.com")}')
print(f'User credentials: {get_user_credentials("user@example.pl")}')


# Excerise 3
def print_gamer_description(gamer_dictionary):
    print(
        f"The player {gamer_dictionary['nick']} is of type {gamer_dictionary['type']} and has {gamer_dictionary['exp_points']} EXP")


player_1 = {
    "nick": "maestro_54",
    "type": "warrior",
    "exp_points": 3000
}
print_gamer_description(player_1)
