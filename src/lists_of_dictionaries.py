animal = {
    "kind": "dog",
    "age": 2,
    "male": True
}
animal2 = {
    "kind": "cat",
    "age": 5,
    "male": False
}

animals = ["dog", "cat", "fish"]

animals = [
    {
        "kind": "dog",
        "age": 2,
        "male": True
    },
    {
        "kind": "cat",
        "age": 5,
        "male": False
    },
    {
        "kind": "fish",
        "age": 1,
        "male": True
    }
]
print(len(animals))
print(animals[0])
print(animals[-1])
last_animal = animals[-1]
last_animal_age = last_animal["age"]
print("The age of last animal is equal to", last_animal_age)
print(animals[0]["male"])
print(animals[1]["kind"])

animals.append(
    {
        "kind": "zebra",
        "age": 7,
        "male": False
    }
)
print(animals)


def get_employee_info(email):
    return {
        'employee_email': email,
        'company': '4testers.pl'
    }


if __name__ == '__main__':

    info_1 = get_employee_info('robert@4testers.pl')
    info_2 = get_employee_info('marta@4testers.pl')
    info_3 = get_employee_info('zosia@4testers.pl')

    print(info_1)
    print(info_2)
    print(info_3)
